// console.log("HI")

// Objects
	//  used to represent real world objects
	// information in object are stored in "key: value" pair

// Create object using object initializers / literal notation
/*
	let objectName = {
		keyA: valueA,
		keyB: valueB
	}
*/

let cellphone = {
	name: "Noakia 3210",
	manufactureDate: 1999
};

console.log('Object created through initializer:')
console.log(cellphone);
console.log(typeof cellphone);

// Create object using constructor function
/*
	Syntax:
		function objectName(keyA, keyB){
			this.keyA = keyA;
			this.keyB = keyB;
		}
*/

function Laptop(name, manufactureDate){
	this.name = name;
	this.manufactureDate = manufactureDate;
};

let laptop = new Laptop('Lenovo', 2008)
console.log('Object through constructor function:')
console.log(laptop);

let myLaptop = new Laptop('Macbook Air', 2020)
console.log('Object through constructor function again:')
console.log(myLaptop);

let oldLaptop = Laptop('Portal R2E CCMC', 1980);
console.log('Object through constructor function without new keyword:')
console.log(oldLaptop);
//result: undefined

// Creating empty objects
let computer = {};
let myComptuer = new Object();

// Accesing object properties
	// >>INITIALIZERS

// using dot notation
console.log('Result from dot notation: ' + myLaptop.name)

// using square bracket notation
console.log('Result from bracket notation: ' + myLaptop['name'])

// simple initializer
console.log(`Result from dot notation: ${laptop.name}`);

console.log(`Result from dot notation with non-existing property: ${laptop.specs}`);
// Result from dot notation with non-existing property: undefined


// Accessing array object

let array = [laptop, myLaptop];

// bracket notation
console.log(`Result from bracket notation: ${array[0]['name']}`)
console.log(`Result from bracket notation: ${array[1].manufactureDate}`)

/*
	Mini-Activity
		>>Create an object with the following key:value pair
			firstName: string
			lastName: string
			passWord: string
			email: string
			age: number
		>>Access each property`s values
			>>5 console.log showing all values of the object
*/

let personDetails = {
	firstName: 'Ippo',
	lastName: 'Makunochi',
	passWord: '123123',
	email: 'ippomakunochi.com',
	age: 25
};

console.log(`firstName: ${personDetails.firstName}`)
console.log(`lastName: ${personDetails.lastName}`)
console.log(`passWord: ${personDetails.passWord}`)
console.log(`email: ${personDetails.email}`)
console.log(`age: ${personDetails.age}`)
// Initialize / add object properties using dot notation

let car = {};
car.name = 'Honda Civic';
console.log('Result from adding property through dot notation:');
console.log(car);

// using bracket notation

car['manufacture date'] = 2019
console.log(`Result from adding property through bracket notation:`);
console.log(car);

// deleting objects properties
delete car['manufacture date'];
console.log('Result from deleting:');
console.log(car);

// re-assign object properties
car.name = 'Dodge Charger R/T'
console.log('Result from reassignment:');
console.log(car);

// Object Methods

let person1 = {
	name: 'John',
	talk: function(){
		console.log(`Hello my name is ${this.name}`);
	}
};

console.log(person1);
console.log('Result from object method')
person1.talk();

//  adding methods to our objects
person1.walk = function(){
	console.log(`${this.name} walked 25 steps forward`);
};
person1.walk();

let friend = {
	firstName: "Joe",
	lastName: "Smith",
	address:{
		city: 'Austin',
		country: 'Texas'
	},
	email: ['joe@mail.com', 'joesmith@email.xyz'],
	introduce: function(){
		console.log(`Hello my name is ${this.firstName} ${this.lastName}. live at ${this.address.city}, ${this.address.country} and my personal email ${this.email[0]}`)
	}
}
friend.introduce();

// Real world application of objects

function Pokemon(name, level){
	this.name = name;
	this.lvl = level;
	this.health = 2 * level;
	this.atk = level;

	// Methods
	this.tackle = function(target){
		console.log(`${this.name} tackled ${target.name}`)
		console.log(`targetPokemons's health is now reduced to _targetPokemonHealth_`);
	};
		this.faint = function(){
			console.log(`${this.name} fainted`)
		};

};

let bulbasaur = new Pokemon("bulbasaur", 16);
let rattata = new Pokemon("rattata", 8);

console.log(bulbasaur);
console.log(rattata);

bulbasaur.tackle(rattata);
rattata.tackle(bulbasaur);
bulbasaur.faint();

